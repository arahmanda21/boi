﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeamIndicator : MonoBehaviour
{
    private float indicatorOffsetY = 0.1f;
    private SpriteRenderer spriteRenderer;

    private Transform parent;
    private Vector3 raycastOffsetPos = new Vector3(0, 1.0f, 0);
    private Vector3 hitPos;

    public LayerMask whatIsGround;

    void Start()
    {
        parent = transform.parent;
    }

    void FixedUpdate()
    {
        Vector3 raycastStartPos = parent.position + raycastOffsetPos;
        Vector3 raycastDirection = -parent.up;

        RaycastHit hit;
        if(Physics.Raycast(raycastStartPos, raycastDirection, out hit, Mathf.Infinity, whatIsGround))
        {
          hitPos = hit.point + new Vector3(0, indicatorOffsetY, 0);
          transform.position = hitPos;
        }
    }

    public void SetIndicatorColor(Color _color)
    {
      if(spriteRenderer == null)
        spriteRenderer = GetComponent<SpriteRenderer>();

      _color.a = 0.5f;
      spriteRenderer.color = _color;
    }
}
