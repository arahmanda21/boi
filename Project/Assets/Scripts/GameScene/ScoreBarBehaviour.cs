﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBarBehaviour : MonoBehaviour
{
    public int value = 0;
    private int maxValue = 1;

    public float offsetX = 10f;
    public bool reverseBar = false;

    public Image originBar;
    private List<Image> bars;

    public bool useColor;
    public Sprite activeBar;
    public Color activeColor = Color.gray;
    private Sprite inactiveBar;

    public void InitBars(int value, int maxValue)
    {
      inactiveBar = originBar.sprite;

      bars = new List<Image>();
      bars.Add(originBar);

      Transform barParent = originBar.transform.parent;
      RectTransform barRectTransform = originBar.GetComponent<RectTransform>();

      Vector2 startPos = barRectTransform.anchoredPosition;
      float barWidth = barRectTransform.rect.width;

      for(int i = 1; i < maxValue; i++)
      {
        Image newBar = Instantiate(originBar) as Image;
        newBar.transform.SetParent(barParent);

        if(!reverseBar)
          newBar.GetComponent<RectTransform>().anchoredPosition = new Vector2(startPos.x + (offsetX + barWidth) * i, startPos.y);
        else
          newBar.GetComponent<RectTransform>().anchoredPosition = new Vector2(startPos.x - (offsetX + barWidth) * i, startPos.y);

        newBar.transform.localScale = Vector3.one;

        bars.Add(newBar);
      }

      this.maxValue = maxValue;
      SetValue(value);
    }

    public void SetValue(int value)
    {
      if(bars != null && value <= maxValue)
      {
        if(value > this.value)
        {
          for(int i = this.value; i < value; i++)
          {
            if(useColor)
              bars[i].color = activeColor;
            else
              bars[i].sprite = activeBar;
          }
        } else {
          for(int i = this.value - 1; i >= value; i--)
          {
            if(useColor)
              bars[i].color = Color.white;
            else
              bars[i].sprite = inactiveBar;
          }
        }
        this.value = value;
      }
      Debug.Log("Score set to " + this.value);
    }
}
