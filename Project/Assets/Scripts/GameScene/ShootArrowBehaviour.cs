using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShootArrowBehaviour : MonoBehaviour
{
    public float offset = 0.5f;
    private float startPos;
    private float maxPos;
    private float value;
    private float maxValue;

    private SpriteRenderer spriteRenderer;

    public Color lowColor;
    public Color highColor;

    void Start()
    {
      spriteRenderer = GetComponent<SpriteRenderer>();
      startPos = transform.localPosition.z;
      maxPos = startPos + offset;
      maxValue = Mathf.Abs(maxPos - startPos);
    }

    public void Show()
    {
      LeanTween.scale(this.gameObject, Vector3.one, 0.2f).setEase(LeanTweenType.easeInOutBounce);
    }

    public void Hide()
    {
      LeanTween.scale(this.gameObject, Vector3.zero, 0.2f).setEase(LeanTweenType.easeInOutBounce);
    }

    public void SetValue(float shootForce, float maxShootForce)
    {
      value = shootForce * maxValue / maxShootForce;
      transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, startPos + value);

      spriteRenderer.color = Color.Lerp(lowColor, highColor, value / maxValue);
    }
}
