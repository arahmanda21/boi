﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIBehaviour : MonoBehaviour
{
    public CanvasGroup canvas;

    public LTDescr OpenCanvas()
    {
      return LeanTween.alphaCanvas(canvas, 1f, 0.5f);
    }

    public LTDescr CloseCanvas()
    {
      return LeanTween.alphaCanvas(canvas, 0f, 0.5f);
    }
}
