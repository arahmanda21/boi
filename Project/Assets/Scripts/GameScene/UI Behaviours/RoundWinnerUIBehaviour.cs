﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RoundWinnerUIBehaviour : UIBehaviour
{
  public TextMeshProUGUI winnerStatusLabel;
  public TextMeshProUGUI[] teamScoreLabel = new TextMeshProUGUI[2];

  public void UpdateWinner(string winnerTeam, Color winnerColor)
  {
    winnerStatusLabel.text = winnerTeam;
    winnerStatusLabel.color = winnerColor;
  }

  public void UpdateScore(int redTeamScore, int blueTeamScore)
  {
    teamScoreLabel[0].text = redTeamScore.ToString();
    teamScoreLabel[1].text = blueTeamScore.ToString();
  }
}
