﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameWinnerUIBehaviour : UIBehaviour
{
    [SerializeField] private TextMeshProUGUI winnerStatusLabel;

    public void UpdateWinner(string winnerTeam, Color winnerColor)
    {
      winnerStatusLabel.text = winnerTeam;
      winnerStatusLabel.color = winnerColor;
    }
}
