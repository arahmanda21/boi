﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CountdownUIBehaviour : UIBehaviour
{
    public TextMeshProUGUI countdownLabel;

    public void UpdateCountdownLabel(int _time)
    {
      countdownLabel.text = _time.ToString();
    }
}
