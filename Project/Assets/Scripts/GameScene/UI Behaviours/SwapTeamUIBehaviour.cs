﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SwapTeamUIBehaviour : UIBehaviour
{
    public TextMeshProUGUI[] roleLabel = new TextMeshProUGUI[2];
    private RectTransform[] roleLabel_RT = new RectTransform[2];

    // Start is called before the first frame update
    void Start()
    {
      for(int i = 0; i < roleLabel_RT.Length; i++)
        roleLabel_RT[i] = roleLabel[i].GetComponent<RectTransform>();
    }

    public void Swap()
    {
      for(int i = 0; i < TeamManager.i.teams.Length; i++)
        roleLabel[i].text = TeamManager.i.teams[i].role;

      Vector2[] tempPos = new Vector2[2];
      for(int i = 0; i < tempPos.Length; i++)
        tempPos[i] = roleLabel_RT[i].anchoredPosition;

      roleLabel_RT[0].anchoredPosition = tempPos[1];
      roleLabel_RT[1].anchoredPosition = tempPos[0];

      LeanTween.moveX(roleLabel_RT[0], tempPos[0].x, 1f).setDelay(1.5f).setEase(LeanTweenType.easeOutCirc);
      LeanTween.moveX(roleLabel_RT[1], tempPos[1].x, 1f).setDelay(1.5f).setEase(LeanTweenType.easeOutCirc);
    }
}
