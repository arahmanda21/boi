﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    private Collider collider;
    private Rigidbody rigidbody;
    private TrailRenderer trailRenderer;

    private Vector3 localScaleOriginal;

    void Awake()
    {
      collider = GetComponent<Collider>();
      rigidbody = GetComponent<Rigidbody>();
      trailRenderer = GetComponent<TrailRenderer>();
      localScaleOriginal = transform.localScale;
    }

    public void HoldedBy(Transform parent)
    {
      transform.parent = parent;
      transform.localPosition = Vector3.zero;
      transform.localEulerAngles = Vector3.zero;

      rigidbody.constraints = RigidbodyConstraints.FreezeAll;
      trailRenderer.emitting = false;
    }

    public void Shoot(Vector3 direction)
    {
      transform.parent = null;
      transform.localScale = localScaleOriginal;
      rigidbody.constraints = RigidbodyConstraints.None;

      rigidbody.AddForce(direction, ForceMode.Impulse);
      trailRenderer.emitting = true;
    }

    public void ResetVelocity()
    {
      rigidbody.velocity = Vector3.zero;
      rigidbody.angularVelocity = Vector3.zero;
    }

    public void IgnoreCollisionAfterTimeout(Collider another, bool value, float timeout)
    {
      StartCoroutine(IgnoreCollision(another, value, timeout));
    }

    IEnumerator IgnoreCollision(Collider another, bool value, float timeout)
    {
      yield return new WaitForSeconds(timeout);
      if(another != null)
        Physics.IgnoreCollision(another, collider, value);
    }
}
