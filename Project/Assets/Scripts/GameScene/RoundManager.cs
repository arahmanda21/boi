using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

public class RoundManager : MonoBehaviour
{
    [Header("General Settings")]
    public int currentRound = 0;
    public int totalRoundWin = 4;
    public int roundTime = 99;
    private int initRoundTime;

    public bool roundStarted;
    public bool roundEnded;

    [SerializeField] private TextMeshProUGUI roundTimeLabel;
    [SerializeField] private TextMeshProUGUI RoundLabel;

    private Team teamWinner;

    [Header("Red Team Settings")]
    public int redTeamScore;
    [SerializeField] private Color redTeamColor = Color.red;

    [Header("Blue Team Settings")]
    public int blueTeamScore;
    [SerializeField] private Color blueTeamColor = Color.blue;

    [Header("Prefabs")]
    public PlayerBehaviours playerBehavioursPrefab;
    public Ball ballPrefab;
    private Ball ball;

    [Header("Spawn Settings")]
    public float spawnOffset = 2f;
    [SerializeField] private Transform[] teamSpawnPos = new Transform[2];
    [SerializeField] private Transform ballSpawnPos;

    [Header("ScoreBar Settings")]
    [SerializeField] private ScoreBarBehaviour[] scoreBars;

    [Header("Default UI Settings")]
    [SerializeField] private CanvasGroup defaultUICanvas;
    [SerializeField] private TextMeshProUGUI[] teamRoleLabel = new TextMeshProUGUI[2];

    [Header("UI Settings")]
    public RoundWinnerUIBehaviour roundWinnerUIBehaviour;
    public GameWinnerUIBehaviour gameWinnerUIBehaviour;
    public SwapTeamUIBehaviour swapTeamUIBehaviour;
    public CountdownUIBehaviour countdownUIBehaviour;

    public bool canCheckGameStates;

    public bool countdownStarted;
    private float countdownTime;
    private int countdown;

    private const float ONE_SECOND = 1f;
    private const float FALL_DEPTH_LIMIT = -35f;

    private List<Player> players;

    private void Start()
    {
      players = new List<Player>();

      for(int i = 0; i < scoreBars.Length; i++)
        scoreBars[i].InitBars(0, totalRoundWin);

      ball = Instantiate(ballPrefab, ballSpawnPos.position, Quaternion.identity) as Ball;

      initRoundTime = roundTime;
      AudioManager.BGMPlay("BGM1", true);

      PauseGame(true);
      GlobalConfiguration.i.gameStarted = false;
      GlobalConfiguration.i.gameEnded = false;
      canCheckGameStates = false;
      roundStarted = false;
      roundEnded = false;

      TeamManager.i.SetTeamRole("Red Team", "Breaker");
      TeamManager.i.SetTeamRole("Blue Team", "Builder");

      StartNewRound();
    }

    private void Update()
    {
      if(GlobalConfiguration.i.gameEnded) return;
        Countdown();

      if(ball != null)
      {
        if(ball.transform.position.y < FALL_DEPTH_LIMIT) ResetBall();
      } else
        ball = Instantiate(ballPrefab, ballSpawnPos.position, Quaternion.identity) as Ball;
    }

    public void StartNewRound()
    {
      //Add new round
      currentRound++;

      //Reset all states
      roundTime = initRoundTime;
      RoundLabel.text = "Round " + currentRound;
      roundTimeLabel.text = roundTime.ToString();
      GameManager.ResetAllState();

      //Update UI
      for(int i = 0; i < TeamManager.i.teams.Length; i++)
        teamRoleLabel[i].text = "As " + TeamManager.i.teams[i].role;

      scoreBars[0].SetValue(redTeamScore);
      scoreBars[1].SetValue(blueTeamScore);

      //Reset scene
      DestroyAllPlayers();
      SpawnPlayers();

      if(ball != null)
        ResetBall();
      else
        ball = Instantiate(ballPrefab, ballSpawnPos.position, Quaternion.identity) as Ball;

      //Start round after countdown
      StartCountdown(3);
    }

    public void EndCurrentRound()
    {
      roundEnded = true;
      roundStarted = false;
      canCheckGameStates = true;
    }

    private void SpawnPlayers()
    {
      foreach(Team team in TeamManager.i.teams)
      {
        foreach(PlayerConfiguration playerConfig in team.playerConfigs)
        {
          Player newPlayer = NewPlayer(playerConfig, team);
          players.Add(newPlayer);
        }
      }

      List<Transform> breakers = GetPlayers<Transform>("Breaker");
      List<Transform> builders = GetPlayers<Transform>("Builder");

      foreach(Player player in players)
      {
        if(player.role == "Breaker")
        {
          List<Transform> allies = new List<Transform>();
          foreach(Transform breaker in breakers)
            if(breaker != player.transform) allies.Add(breaker);
          List<Transform> enemies = builders;
          player.breakerBehaviour.SetTargets(enemies, allies);
        }
      }
    }

    public void DestroyAllPlayers()
    {
      //destroy all spawned players
      if(players.Count > 0)
      {
        foreach(Player player in players)
        {
          if(player != null)
          {
            player.RemoveAllListener();
            Destroy(player.gameObject);
          }
        }
        players.Clear();
      }
    }

    private Player NewPlayer(PlayerConfiguration playerConfig, Team team)
    {
        Player player = Instantiate(playerConfig.character.playerPrefab) as Player;
        player.name = "Player " + (playerConfig.index + 1) + " - " + team.name + " (" + team.role + ")";
        player.transform.position = Tools.RandomizePos(teamSpawnPos[(team.role == "Breaker") ? 0 : 1].position, spawnOffset);
        player.transform.rotation = Quaternion.Euler(0, 180, 0);
        player.teamIndicator.SetIndicatorColor(team.name == "Red Team" ? redTeamColor : blueTeamColor);
        player.SetBehaviour(playerConfig, playerBehavioursPrefab, team.role);

        return player;
    }

    private void Countdown()
    {
      if(countdownTime > 0)
        countdownTime -= Time.deltaTime;
      else
      {
        if(roundStarted)
        {
          roundTime--;
          roundTimeLabel.text = roundTime.ToString();
          if(roundTime == 0)
            SetRoundWinner("");
          else
            countdownTime = ONE_SECOND;
        } else if(countdownStarted) {
          countdown--;
          countdownUIBehaviour.UpdateCountdownLabel(countdown);
          if(countdown == 0)
            EndCountdown();
          else
            countdownTime = ONE_SECOND;
        }
      }
    }

    private void StartCountdown(int _time)
    {
      countdown = _time;
      countdownStarted = true;
      countdownTime = ONE_SECOND;
      countdownUIBehaviour.UpdateCountdownLabel(countdown);
      countdownUIBehaviour.OpenCanvas().setEase(LeanTweenType.easeOutCirc);
    }

    private void EndCountdown()
    {
      //Start round
      GlobalConfiguration.i.gameStarted = true;
      countdownStarted = false;
      roundStarted = true;
      roundEnded = false;
      countdownTime = ONE_SECOND;
      PauseGame(false);

      countdownUIBehaviour.CloseCanvas().setEase(LeanTweenType.easeOutCirc);
      LeanTween.alphaCanvas(defaultUICanvas, 1f, 0.5f).setEase(LeanTweenType.easeOutCirc);
    }

    private void ResetBall()
    {
        ball.transform.position = ballSpawnPos.position;
        ball.ResetVelocity();
    }

    public List<Player> GetPlayers()
    {
      return players;
    }

    public List<Player> GetPlayers(string role)
    {
      List<Player> players = new List<Player>();

      foreach(Player player in this.players)
        if(player.role == role) players.Add(player);

      return players;
    }

    public List<T> GetPlayers<T>(string role)
    {
      List<T> players = new List<T>();

      foreach(Player player in this.players)
        if(player.role == role) players.Add(player.GetComponent<T>());

      return players;
    }

    public void SetRoundWinner(string winnerTeamRole)
    {
      PauseGame(true);
      if(winnerTeamRole == "Breaker" || winnerTeamRole == "Builder" )
      {
        //Set winner
        teamWinner = TeamManager.i.GetTeamByRole(winnerTeamRole);
        teamWinner.score++;

        //Updating winner label
        Color winnerColor = teamWinner.name == "Red Team" ? redTeamColor : blueTeamColor;
        roundWinnerUIBehaviour.UpdateWinner(teamWinner.name, winnerColor);

        //Updating score
        redTeamScore = TeamManager.i.teams[0].score;
        blueTeamScore = TeamManager.i.teams[1].score;
        roundWinnerUIBehaviour.UpdateScore(redTeamScore, blueTeamScore);
      } else {
        roundWinnerUIBehaviour.UpdateWinner("Draw", Color.white);
      }

      EndCurrentRound();

      roundWinnerUIBehaviour.OpenCanvas().setEase(LeanTweenType.easeOutCirc);
    }

    public void SetGameWinner(string winnerTeam)
    {
      GlobalConfiguration.i.gameEnded = true;

      Color winnerColor = winnerTeam == "Red Team" ? redTeamColor : blueTeamColor;
      gameWinnerUIBehaviour.UpdateWinner(winnerTeam, winnerColor);
      gameWinnerUIBehaviour.OpenCanvas().setEase(LeanTweenType.easeOutCirc);
    }

    public void CheckGameStates()
    {
        roundWinnerUIBehaviour.CloseCanvas().setEase(LeanTweenType.easeOutCirc);
        if(redTeamScore >= totalRoundWin || blueTeamScore >= totalRoundWin)
        {
          string gameWinner = redTeamScore > blueTeamScore ? "Red Team" : "Blue Team";
          SetGameWinner(gameWinner);
        }
        else
        {
          StartCoroutine(SwapTeamRole());
        }
        canCheckGameStates = false;
    }

    IEnumerator SwapTeamRole()
    {
      TeamManager.i.SwapTeamRole();

      swapTeamUIBehaviour.OpenCanvas().setEase(LeanTweenType.easeOutCirc);
      swapTeamUIBehaviour.Swap();

      yield return new WaitForSeconds(5f);

      swapTeamUIBehaviour.CloseCanvas().setEase(LeanTweenType.easeInCirc);
      StartNewRound();
    }

    private void PauseGame(bool value)
    {
      if(value)
      {
        GlobalConfiguration.i.gamePaused = true;

        foreach(Player player in players)
        {
          player.movementBehaviour.SetMove(Vector2.zero);
          if(player.role == "Builder")
            player.builderBehaviour.CancelBuild();
        }
      } else {
          GlobalConfiguration.i.gamePaused = false;
      }
    }
}
