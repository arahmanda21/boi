﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(RoundManager))]
public class GameManager : MonoBehaviour
{
    public static RoundManager roundManager;

    public static BuildBarBehaviour buildBar;

    public static GameManager instance;

    private static bool builderWin;
    private static int builderLeft;

    private static bool breakerWin;
    private static int breakerLeft;

    void Awake()
    {
      buildBar = FindObjectOfType<BuildBarBehaviour>();
      roundManager = GetComponent<RoundManager>();
    }

    void Start()
    {
      PlayerConfigurationManager.i.SetPlayerInputAction("Game");
    }

    void Update()
    {
      if(GlobalConfiguration.i.gameEnded) return;

      if(!builderWin && breakerWin)
      {
        roundManager.SetRoundWinner("Breaker");
        builderWin = false;
        breakerWin = false;
      }

      if(!breakerWin && builderWin)
      {
        roundManager.SetRoundWinner("Builder");
        builderWin = false;
        breakerWin = false;
      }
    }

    public static void BuildComplete()
    {
      builderWin = true;
    }

    public static void BreakerDead(Player deadPlayer)
    {
      breakerLeft--;
      foreach(Player player in roundManager.GetPlayers("Breaker"))
        player.breakerBehaviour.RemoveTarget("Ally", deadPlayer.transform);
      Debug.Log("Breaker : " + breakerLeft);

      if(breakerLeft == 0)
        builderWin = true;
    }

    public static void BuilderDead(Player deadPlayer)
    {
      builderLeft--;
      foreach(Player player in roundManager.GetPlayers("Breaker"))
        player.breakerBehaviour.RemoveTarget("Enemy", deadPlayer.transform);
      Debug.Log("Builder : " + builderLeft);

      if(builderLeft == 0)
        breakerWin = true;
    }

    public static void ResetAllState()
    {
      builderLeft = TeamManager.i.GetPlayersCountByRole("Builder");
      breakerLeft = TeamManager.i.GetPlayersCountByRole("Breaker");
      buildBar.SetValue(0, 3);
      Debug.Log("Builder : " + builderLeft + ", " + "Breaker : " + breakerLeft);
      builderWin = false;
      breakerWin = false;
    }
}
