using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Player))]
public class BreakerBehaviour : MonoBehaviour
{
    private Player player;
    private MovementBehaviour movementBehaviour;
    private BreakerInputHandler breakerInputHandler;

    [Header("Behaviour Settings")]
    public Transform holdPoint;
    public ShootArrowBehaviour shootArrow;

    public Vector2 throwingForce;
    public Vector2 vertThrowingForce;
    public float forceResult;

    public float initShootTime;
    public float shootTime;

    [SerializeField] private Collider collider;
    private Ball ball = null;

    public List<Transform> enemies;
    private int currentEnemyTarget = 0;
    public List<Transform> allies;
    private int currentAllyTarget = 0;

    private Vector3 targetPos;
    private bool isPass;

    public bool holdingBall = false;
    private bool calculateShootForce = false;

    void Awake()
    {
        player = GetComponent<Player>();
        movementBehaviour = player.movementBehaviour;
    }

    public void FindMissingReferences()
    {
      collider = GetComponentInChildren<Collider>();
      shootArrow = GetComponentInChildren<ShootArrowBehaviour>();
      holdPoint = Tools.GetChildTransformWithName(transform, "HoldPoint");
    }

    void Update()
    {
      if(GlobalConfiguration.i.gameEnded) return;

      if(calculateShootForce)
        ShootForceCalculation();
    }

    public void AddInputListener()
    {
      breakerInputHandler = gameObject.AddComponent<BreakerInputHandler>();
      breakerInputHandler.SetPlayer(player);
      player.config.input.onActionTriggered += breakerInputHandler.HandleAction;
    }

    public void RemoveInputListener()
    {
      player.config.input.onActionTriggered -= breakerInputHandler.HandleAction;
      Destroy(breakerInputHandler);
    }

    public void SetTargets(List<Transform> enemies, List<Transform> allies)
    {
      this.enemies = enemies;
      this.allies = allies;
    }

    public void HoldTheBall()
    {
        ball.HoldedBy(holdPoint);
        ball.IgnoreCollisionAfterTimeout(collider, true, 0);

        holdingBall = true;
    }

    public void CalculateShoot() {
      if(holdingBall) {
          currentEnemyTarget = GetNearestTarget(enemies);

          // show shoot bar
          shootArrow.Show();

          // shoot mode on
          isPass = false;
          movementBehaviour.UseDefaultMovement = false;
          calculateShootForce = true;
          shootTime = initShootTime;
        }
    }

    private void ShootForceCalculation(){
      if(holdingBall) {
        if(isPass)
        {
          if(allies != null && allies.Count != 0)
            targetPos = allies[currentAllyTarget].position;
        } else
        {
          if(enemies != null && enemies.Count != 0)
            targetPos = enemies[currentEnemyTarget].position;
        }

        movementBehaviour.SetTarget(targetPos);

        if(shootTime > 0)
          shootTime -= Time.deltaTime;

        forceResult = throwingForce.y * (1 - (shootTime / initShootTime));

        shootArrow.SetValue(forceResult, throwingForce.y);
      }
    }

    public void ShootTheBall() {
      if(holdingBall) {
        // hide shoot bar
        shootArrow.Hide();

        // shoot
        if(forceResult < throwingForce.x)
            Shoot(throwingForce.x, Random.Range(vertThrowingForce.x, vertThrowingForce.y));
        else
            Shoot(forceResult, Random.Range(vertThrowingForce.x, vertThrowingForce.y));
        AudioManager.SFXPlay("Shoot");

        // shoot mode off
        shootTime = initShootTime;
        movementBehaviour.UseDefaultMovement = true;
        calculateShootForce = false;
        holdingBall = false;
      }
    }

    void Shoot(float force, float vertForce)
    {
      Vector3 targetdir = (targetPos - transform.position).normalized;
      ball.Shoot(targetdir * force + Vector3.up * vertForce);
      ball.IgnoreCollisionAfterTimeout(collider, false, 1f);
    }

    private int GetNearestTarget(List<Transform> targets)
    {
      if(targets == null)
        return 0;

      float nearestDistance = Vector3.Distance(this.transform.position, targets[0].position);
      int nearestTargetIndex = 0;
      for(int i = 1; i < targets.Count; i++)
      {
        float targetDistance = Vector3.Distance(this.transform.position, targets[i].position);
        if(targetDistance < nearestDistance)
        {
          nearestTargetIndex = i;
          nearestDistance = targetDistance;
        }
      }

      return nearestTargetIndex;
    }

    public void SwitchTarget(string targetLabel)
    {
      if(holdingBall)
      {
        if(targetLabel == "Ally")
        {
          if(allies != null && allies.Count != 0)
          {
            isPass = true;
            currentAllyTarget = SwitchNextTarget(allies, currentAllyTarget);
          }
        } else if(targetLabel == "Enemy")
        {
          if(enemies != null && enemies.Count != 0)
          {
            isPass = false;
            currentEnemyTarget = SwitchNextTarget(enemies, currentEnemyTarget);
          }
        }
      }
    }

    private int SwitchNextTarget(List<Transform> targets, int currentIndex)
    {
        int nextTarget;
        if(currentIndex == (targets.Count - 1))
          nextTarget = 0;
        else
          nextTarget = currentEnemyTarget + 1;

        return nextTarget;
    }

    public void RemoveTarget(string targetLabel, Transform target)
    {
      if(targetLabel == "Ally")
        allies.Remove(target);
      else if(targetLabel == "Enemy")
        enemies.Remove(target);
    }

    private void OnCollisionEnter(Collision other)
    {
      if (other.gameObject.tag == "Ball" && !holdingBall) {
          ball = other.gameObject.GetComponent<Ball>();
          HoldTheBall();
      }
    }
}
