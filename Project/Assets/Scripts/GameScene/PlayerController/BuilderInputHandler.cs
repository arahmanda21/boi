﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class BuilderInputHandler : MonoBehaviour
{
  private Player player;
  private MovementBehaviour movementBehaviour;
  private BuilderBehaviour builderBehaviour;

  public void SetPlayer(Player player)
  {
    this.player = player;
    movementBehaviour = player.movementBehaviour;
    builderBehaviour = player.builderBehaviour;
  }

  public void HandleAction(InputAction.CallbackContext ctx)
  {
    if(!GlobalConfiguration.i.gameEnded)
    {
      if(!GlobalConfiguration.i.gamePaused)
      {
        if(movementBehaviour != null)
        {
          if(ctx.action.name == "Move")
          {
            if(ctx.performed)
              movementBehaviour.SetMove(ctx.ReadValue<Vector2>());

            if(ctx.canceled)
              movementBehaviour.Stop();
          }

          if(ctx.action.name == "Cross" && ctx.performed)
            movementBehaviour.Jump();
        }

        if(builderBehaviour != null)
        {
          if(ctx.action.name == "Rectangle")
          {
            if(ctx.performed)
              builderBehaviour.Build();

            if(ctx.canceled)
              builderBehaviour.CancelBuild();
          }
        }

        if(ctx.action.name == "Start" && ctx.performed)
          GlobalConfiguration.i.gamePaused = true;
      } else {
        if(GameManager.roundManager.roundEnded && GameManager.roundManager.canCheckGameStates)
        {
          if(ctx.action.name == "Cross" && ctx.performed)
            GameManager.roundManager.CheckGameStates();
        }

        if(ctx.action.name == "Start" && ctx.performed)
          GlobalConfiguration.i.gamePaused = false;
      }
    }
    else
    {
      if(ctx.action.name == "Start" && ctx.performed)
      {
        DDOL.Destroy();
        PlayerConfigurationManager.i.SetPlayerInputAction("Menu");
        AudioManager.BGMPlay("Lucid", true);
        SceneManager.LoadScene("MainMenu");
      }
    }
  }
}
