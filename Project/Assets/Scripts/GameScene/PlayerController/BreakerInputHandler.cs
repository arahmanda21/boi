using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class BreakerInputHandler : MonoBehaviour
{
  private Player player;
  private MovementBehaviour movementBehaviour;
  private BreakerBehaviour breakerBehaviour;

  public void SetPlayer(Player player)
  {
    this.player = player;
    movementBehaviour = player.movementBehaviour;
    breakerBehaviour = player.breakerBehaviour;
  }

  public void HandleAction(InputAction.CallbackContext ctx)
  {
    if(!GlobalConfiguration.i.gameEnded)
    {
      if(!GlobalConfiguration.i.gamePaused)
      {
        if(movementBehaviour != null)
        {
          if(ctx.action.name == "Move")
          {
            if(ctx.performed)
              movementBehaviour.SetMove(ctx.ReadValue<Vector2>());

            if(ctx.canceled)
              movementBehaviour.Stop();
          }

          if(ctx.action.name == "Cross" && ctx.performed)
              movementBehaviour.Jump();
        }

        if(breakerBehaviour != null)
        {
          if(ctx.action.name == "Rectangle")
          {
            if(ctx.performed)
              breakerBehaviour.CalculateShoot();

            if(ctx.canceled)
              breakerBehaviour.ShootTheBall();
          }

          if(ctx.action.name == "RightShoulder" && ctx.performed)
            breakerBehaviour.SwitchTarget("Enemy");

          if(ctx.action.name == "LeftShoulder" && ctx.performed)
            breakerBehaviour.SwitchTarget("Ally");
        }

        if(ctx.action.name == "Start" && ctx.performed)
            GlobalConfiguration.i.gamePaused = true;
      } else {
        if(GameManager.roundManager.roundEnded && GameManager.roundManager.canCheckGameStates)
        {
          if(ctx.action.name == "Cross" && ctx.performed)
            GameManager.roundManager.CheckGameStates();
        }

        if(ctx.action.name == "Start" && ctx.performed)
          GlobalConfiguration.i.gamePaused = false;
      }
    }
    else
    {
      if(ctx.action.name == "Start" && ctx.performed)
      {
        DDOL.Destroy();
        PlayerConfigurationManager.i.SetPlayerInputAction("Menu");
        AudioManager.BGMPlay("Lucid", true);
        SceneManager.LoadScene("MainMenu");
      }
    }
  }
}
