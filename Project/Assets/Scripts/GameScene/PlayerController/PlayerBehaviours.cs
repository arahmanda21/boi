﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviours : MonoBehaviour
{
    public MovementBehaviour movementBehaviour;
    public BreakerBehaviour breakerBehaviour;
    public BuilderBehaviour builderBehaviour;
}
