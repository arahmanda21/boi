using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Player))]
public class MovementBehaviour : MonoBehaviour
{
    public float moveSpeed = 3.5f;
    public float rotSpeed = 10.0f;
    public float jumpHeight = 5.0f;
    public float GroundDistance = 0.2f;
    public float DashDistance = 5f;

    private Rigidbody rigidbody;
    private Vector3 moveInput;
    private Vector2 moveDir;
    private Vector3 target;
    public bool UseDefaultMovement = true;

    public Transform feet;
    public bool isGrounded;
    public LayerMask Ground;

    public Animator anim;
    private bool toggleOnce;

    void FixedUpdate() {
       CharacterMovement();
    }

    public void FindMissingReferences()
    {
      rigidbody = GetComponent<Rigidbody>();
      anim = GetComponentInChildren<Animator>();
      feet = Tools.GetChildTransformWithName(transform, "Foot");
    }

    private void CharacterMovement() {
        //Check is grounded
        isGrounded = Physics.CheckSphere(feet.position, GroundDistance, Ground, QueryTriggerInteraction.Ignore);

        if(!isGrounded)
        {
          toggleOnce = true;
        }

        if(isGrounded && toggleOnce)
        {
          anim.SetBool("isJump", false);
          toggleOnce = false;
          Debug.Log("Grounded");
        }

        moveInput = new Vector3(moveDir.x, 0, moveDir.y);

        if(UseDefaultMovement)
        {
          if(moveInput != Vector3.zero)
          {
            Quaternion rotTarget = Quaternion.LookRotation(moveInput);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotTarget, rotSpeed * Time.deltaTime);
          }
        } else {
          transform.LookAt(target);
        }

        if(moveInput != Vector3.zero)
        {
          //Set animation
          anim.SetBool("isWalk", true);
          anim.SetBool("isIdle", false);

          rigidbody.MovePosition(transform.position + moveInput * moveSpeed * Time.fixedDeltaTime);
        }
    }

    public void Jump()
    {
      if(isGrounded)
      {
        rigidbody.velocity += jumpHeight * Vector3.up;
        anim.SetBool("isJump", true);
        AudioManager.SFXPlay("Jump");
        Debug.Log("Jump");
      }
    }

    public void Dash()
    {
        Vector3 dashVelocity = Vector3.Scale(transform.forward, DashDistance * new Vector3((Mathf.Log(1f / (Time.deltaTime * rigidbody.drag + 1)) / -Time.deltaTime), 0, (Mathf.Log(1f / (Time.deltaTime * rigidbody.drag + 1)) / -Time.deltaTime)));
        rigidbody.AddForce(dashVelocity, ForceMode.VelocityChange);
    }

    public void SetMove(Vector2 v2){
      moveDir = v2;
    }

    public void Stop()
    {
        moveDir = Vector2.zero;
        moveInput = Vector3.zero;

        //Set animation
        anim.SetBool("isIdle", true);
        anim.SetBool("isWalk", false);
    }

    public void SetTarget(Vector3 position)
    {
      target = new Vector3(position.x, transform.position.y, position.z);
    }
}
