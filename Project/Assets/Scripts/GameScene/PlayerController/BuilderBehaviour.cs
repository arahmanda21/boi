﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Player))]
public class BuilderBehaviour : MonoBehaviour
{
    private Player player;
    private BuilderInputHandler builderInputHandler;

    [Header("Behaviour Settings")]
    private BuildBarBehaviour buildBar;

    public bool buildMode;
    public bool isBuild;

    void Awake(){
      player = GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
      if(GlobalConfiguration.i.gameEnded) return;

      if(buildMode && isBuild)
        FillBuildBar();
    }

    public void FindMissingReferences()
    {
      buildBar = GameManager.buildBar;
    }

    public void AddInputListener()
    {
      builderInputHandler = gameObject.AddComponent<BuilderInputHandler>();
      builderInputHandler.SetPlayer(player);
      player.config.input.onActionTriggered += builderInputHandler.HandleAction;
    }

    public void RemoveInputListener()
    {
      player.config.input.onActionTriggered -= builderInputHandler.HandleAction;
      Destroy(builderInputHandler);
    }

    public void Build()
    {
      isBuild = true;
      if(buildMode)
        buildBar.isIdle = false;
    }

    public void CancelBuild()
    {
      isBuild = false;
      buildBar.isIdle = true;
    }

    void FillBuildBar()
    {
      if(buildBar.value < buildBar.maxValue)
      {
        buildBar.value += Time.deltaTime;
        buildBar.SetValue(buildBar.value, buildBar.maxValue);
      }
    }

    private void OnCollisionEnter(Collision other)
    {
      if(other.gameObject.tag == "Ball")
      {
        GameManager.BuilderDead(player);
        player.Die();
      }
    }

    void OnTriggerEnter(Collider other)
    {
      if(other.gameObject.tag == "BuildArea")
        buildMode = true;
    }

    void OnTriggerExit(Collider other)
    {
      if(other.gameObject.tag == "BuildArea")
      {
        buildBar.isIdle = true;
        buildMode = false;
      }
    }
}
