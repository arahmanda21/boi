﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class ThrowerInputHandler : MonoBehaviour
{
  private Player player;
  private MovementBehaviour movementBehaviour;
  private ThrowerBehaviour throwerBehaviour;

  public void SetPlayer(Player player)
  {
    this.player = player;
    movementBehaviour = player.movementBehaviour;
    throwerBehaviour = player.throwerBehaviour;
  }

  public void HandleAction(InputAction.CallbackContext ctx)
  {
    if(!GlobalConfiguration.i.gameEnded)
    {
      if(!GlobalConfiguration.i.gamePaused)
      {
        if(movementBehaviour != null)
        {
          if(ctx.action.name == "Move")
          {
            if(ctx.performed)
              movementBehaviour.SetMove(ctx.ReadValue<Vector2>());

            if(ctx.canceled)
              movementBehaviour.Stop();
          }

          if(ctx.action.name == "Cross" && ctx.performed)
              movementBehaviour.Jump();
        }

        if(throwerBehaviour != null)
        {
          if(ctx.action.name == "Rectangle")
          {
            if(ctx.performed)
              throwerBehaviour.CalculateShoot();

            if(ctx.canceled)
              throwerBehaviour.ShootTheBall();
          }

          if(ctx.action.name == "RightShoulder" && ctx.performed)
            throwerBehaviour.SwitchTarget("Enemy");

          if(ctx.action.name == "LeftShoulder" && ctx.performed)
            throwerBehaviour.SwitchTarget("Ally");
        }

        if(ctx.action.name == "Start" && ctx.performed)
            GlobalConfiguration.i.gamePaused = true;
      } else {
        if(movementBehaviour != null)
          movementBehaviour.SetMove(Vector2.zero);

        if(ctx.action.name == "Start" && ctx.performed)
          GlobalConfiguration.i.gamePaused = false;
      }
    }
    else
    {
      if(ctx.action.name == "Start" && ctx.performed)
      {
        DDOL.Destroy();
        PlayerConfigurationManager.i.SetPlayerInputAction("Menu");
        AudioManager.BGMPlay("Lucid", true);
        SceneManager.LoadScene("MainMenu");
      }
    }
  }
}
