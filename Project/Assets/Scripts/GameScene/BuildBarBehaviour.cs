﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildBarBehaviour : MonoBehaviour
{
    public Transform spriteMask;
    public float value;
    public float maxValue = 3f;
    public bool isIdle;

    void Start(){
      spriteMask.localScale = new Vector3((value / maxValue) / 1f, 1f, 1f);
    }

    void Update()
    {
        if(value >= maxValue)
        {
          GameManager.BuildComplete();
          value = 0;
        }

        if(isIdle && value > 0)
        {
          value = value - Time.deltaTime;
          SetValue(value, maxValue);
        }
    }

    public void SetValue(float val, float maxVal){
      spriteMask.localScale = new Vector3((val / maxVal) / 1f, 1f, 1f);
    }
}
