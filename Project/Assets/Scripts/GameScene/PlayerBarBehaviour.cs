using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBarBehaviour : MonoBehaviour
{
    public Transform[] playerBars;
    public Transform[] parentTransform;
    public Vector3[] heights;
    public float offset;

    void Update()
    {
      for(int i = 0; i < playerBars.Length; i++)
      {
        if(parentTransform[i] != null)
          SetPosition(playerBars[i], parentTransform[i].position + heights[i]);
      }
    }

    public void SetPosition(Transform trans, Vector3 parentPosition){
        trans.position = Camera.main.WorldToScreenPoint(parentPosition + (Vector3.up * offset));
    }
}
