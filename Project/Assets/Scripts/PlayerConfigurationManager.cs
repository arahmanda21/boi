﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerConfigurationManager : MonoBehaviour
{
    public const int MAX_PLAYERS = 4;

    public List<PlayerConfiguration> playerConfigs;

    public static PlayerConfigurationManager i;

    void Awake()
    {
      if(i != null)
        Destroy(this);
      else
        i = this;
    }

    public void OnPlayerJoined(PlayerInput playerInput) {
        if(playerConfigs == null)
          playerConfigs = new List<PlayerConfiguration>();

        int newPlayerIndex = playerInput.playerIndex;

        if(!playerConfigs.Any(playerConfig => playerConfig.index == newPlayerIndex))
        {
          GameObject newGameObject = new GameObject("Player Configuration  " + (newPlayerIndex + 1));
          newGameObject.transform.SetParent(transform);

          PlayerConfiguration newPlayerConfig = newGameObject.AddComponent<PlayerConfiguration>();
          newPlayerConfig.SetInput(playerInput);

          playerInput.gameObject.name = "Player " + (newPlayerIndex + 1) + " Input";
          playerInput.transform.SetParent(newGameObject.transform);

          playerConfigs.Add(newPlayerConfig);

          PlayerSetupManager.i.playerSetupController[newPlayerIndex].InitializeSetup(newPlayerConfig);
          Debug.Log("Player " + (newPlayerIndex + 1) + " is joined!!");
        }
    }

    public int GetJoinedPlayers()
    {
        return playerConfigs.Count;
    }

    public bool AllPlayersIsReady()
    {
      if(playerConfigs.Any(playerConfig => !playerConfig.isReady))
        return false;
      else
        return true;
    }

    public void SetCharacter(int index, Character character)
    {
      playerConfigs[index].character = character;
      Debug.Log("Player " + (index + 1) + " selected " + character.name);
    }

    public void SetPlayerInputAction(string inputActionName)
    {
      if(inputActionName != "Menu" && inputActionName != "Game") return;

      foreach(PlayerConfiguration playerConfig in playerConfigs)
        playerConfig.input.SwitchCurrentActionMap(inputActionName);

      Debug.Log("Action Map Changed To " + inputActionName);
    }

    public PlayerInput GetPlayerInput(int index)
    {
      return playerConfigs[index].input;
    }
}
