﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TeamSelectManager : MonoBehaviour
{
  [Header("Players Text")]
  public TextMeshProUGUI playerText;
  private RectTransform[] playerTexts = new RectTransform[4];

  public static int redTeamPlayers;
  public static int blueTeamPlayers;

  [Header("Positions List")]
  public PositionList playerPosList = new PositionList(4);
  public PositionList[] teamPosList = new PositionList[2] { new PositionList(2), new PositionList(2) };

  public static TeamSelectManager i;

  void Awake()
  {
    if(i != null)
      Destroy(this);
    else
      i = this;
  }

  void Start()
  {
    TeamManager.i.InitializeTeam("Red Team", "Blue Team");
    playerPosList.GeneratePositionList(35f);
    for(int i = 0; i < teamPosList.Length; i++)
      teamPosList[i].GeneratePositionList(35f);
  }

  public void NewPlayerText(int playerIndex)
  {
    TextMeshProUGUI newPlayerText = Instantiate(playerText) as TextMeshProUGUI;
    newPlayerText.gameObject.name = "Player " + (playerIndex + 1) + " Text";
    newPlayerText.text = "Player " + (playerIndex + 1);

    playerTexts[playerIndex] = newPlayerText.GetComponent<RectTransform>();
    playerTexts[playerIndex].transform.SetParent(playerPosList.origin.transform);
    playerTexts[playerIndex].anchoredPosition = playerPosList.positions[playerIndex];
  }

  public void SelectTeam(PlayerConfiguration playerConfig, string teamName)
  {
    if(teamName != "Red Team" && teamName != "Blue Team") return;

    playerConfig.selectedTeam = teamName;
    UpdatePlayerTexts();
  }

  public void RemovePlayerConfig(PlayerConfiguration playerConfig, string teamName)
  {
    if(teamName != "Red Team" && teamName != "Blue Team") return;

    playerConfig.selectedTeam = "None";
    UpdatePlayerTexts();
  }

  private void UpdatePlayerTexts()
  {
    UpdatePlayerTexts("Red Team");
    UpdatePlayerTexts("Blue Team");
    UpdatePlayerTexts("None");
  }

  private void UpdatePlayerTexts(string teamName)
  {
    int listIndex = 0;
    if(teamName == "None")
    {
      foreach(PlayerConfiguration playerConfig in PlayerConfigurationManager.i.playerConfigs)
      {
        if(playerConfig.selectedTeam == "None")
        {
            playerTexts[playerConfig.index].transform.SetParent(playerPosList.origin.transform);
            playerTexts[playerConfig.index].anchoredPosition = playerPosList.positions[listIndex];
            Debug.Log("Player " + (playerConfig.index + 1) + " is not in Team");
            listIndex++;
        }
      }
    } else {
      int teamIndex = (teamName == "Red Team") ? 0 : 1;
      foreach(PlayerConfiguration playerConfig in PlayerConfigurationManager.i.playerConfigs)
      {
        if(playerConfig.selectedTeam == teamName)
        {
            playerTexts[playerConfig.index].transform.SetParent(teamPosList[teamIndex].origin.transform);
            playerTexts[playerConfig.index].anchoredPosition = teamPosList[teamIndex].positions[listIndex];
            Debug.Log("Player " + (playerConfig.index + 1) + " is in " + teamName);
            listIndex++;
        }
      }

      if(teamIndex == 0)
        redTeamPlayers = listIndex;
      else
        blueTeamPlayers = listIndex;

      Debug.Log(teamName + " playerConfigs = " + (teamIndex == 0 ? redTeamPlayers : blueTeamPlayers));
    }
  }

  public bool AllTeamIsReady()
  {
    if(redTeamPlayers > 0 && blueTeamPlayers > 0)
    {
      foreach(PlayerConfiguration playerConfig in PlayerConfigurationManager.i.playerConfigs)
        if(playerConfig.selectedTeam == "None") return false;
      return true;
    } else
      return false;
  }

  public void SetSelectedTeam()
  {
    foreach(PlayerConfiguration playerConfig in PlayerConfigurationManager.i.playerConfigs)
      TeamManager.i.AddPlayerToTeam(playerConfig);
  }
}

[System.Serializable]
public class PositionList
{
  public RectTransform origin = null;
  public Vector3[] positions;

  public PositionList(int totalPos)
  {
    positions = new Vector3[totalPos];
  }

  public void GeneratePositionList(float offsetY)
  {
      Vector3 startPos = origin.anchoredPosition;
      for(int i = 0; i < positions.Length; i++)
      {
        positions[i] = new Vector3(startPos.x, startPos.y + (-offsetY * (i + 1)), startPos.z);
      }
  }
}
