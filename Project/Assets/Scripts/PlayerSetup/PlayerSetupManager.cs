﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class PlayerSetupManager : MonoBehaviour
{
    public PlayerSetupController[] playerSetupController;
    public Character[] characters;

    public int currentStep;
    public const int MAX_STEP = 3;

    public Canvas characterSelectCanvas;
    public Canvas teamSelectCanvas;

    public TextMeshProUGUI continueInstruction;
    private bool allowContinue;

    public static PlayerSetupManager i;

    private void Awake()
    {
      if(i != null)
        Destroy(this);
      else
        i = this;
    }

    private void Start()
    {
      continueInstruction.text = "";
    }

    public void SetNextStep()
    {
      if(currentStep < MAX_STEP)
      {
        currentStep++;
        switch(currentStep)
        {
          case 1:
            characterSelectCanvas.enabled = false;
            teamSelectCanvas.enabled = true;
            break;
          case 2:
            for(int i = 0; i < PlayerConfigurationManager.i.GetJoinedPlayers(); i++)
              playerSetupController[i].RemoveAllListener();
            SceneManager.LoadScene("GameScene");
            break;
        }
      }
    }

    public void SetPreviousStep()
    {
      if(currentStep > 0)
      {
        currentStep--;
        switch(currentStep)
        {
          case 0:
            characterSelectCanvas.enabled = true;
            teamSelectCanvas.enabled = false;
            break;
          case 1:
            characterSelectCanvas.enabled = false;
            teamSelectCanvas.enabled = true;
            break;
        }
      }
    }

    public void CheckPlayersConfiguration()
    {
      if(currentStep == 0)
      {
        if(PlayerConfigurationManager.i.AllPlayersIsReady() && PlayerConfigurationManager.i.GetJoinedPlayers() >= 2)
        {
          continueInstruction.text = "Press <b>START</b> to continue";
          allowContinue = true;
        } else {
          continueInstruction.text = "";
          allowContinue = false;
        }
      } else if(currentStep == 1)
      {
        if(TeamSelectManager.i.AllTeamIsReady())
        {
          continueInstruction.text = "Press <b>START</b> to start the game";
          allowContinue = true;
        } else {
          continueInstruction.text = "";
          allowContinue = false;
        }
      }
    }

    public bool ContinueAllowed()
    {
      return allowContinue;
    }
}

[System.Serializable]
public class Character
{
  public string name = "Default";
  public Player playerPrefab;
  public GameObject characterModel;
}
