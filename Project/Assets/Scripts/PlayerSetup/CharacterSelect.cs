﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSelect : MonoBehaviour
{
    public int playerIndex;
    private GameObject playerCharacter;

    private int recentCharacter;
    [SerializeField] private Transform cameraView;

    public void Initialize(int playerIndex)
    {
      this.playerIndex = playerIndex;
      SetCharacter(playerIndex);
    }

    public void SetCharacter(int characterIndex)
    {
      if(PlayerSetupManager.i.characters == null && cameraView == null) return;

      Character characterSelected = PlayerSetupManager.i.characters[characterIndex];
      PlayerConfigurationManager.i.SetCharacter(playerIndex, characterSelected);

      Destroy(playerCharacter);
      playerCharacter = Instantiate(characterSelected.characterModel);
      playerCharacter.transform.SetParent(cameraView);
      playerCharacter.transform.localPosition = new Vector3(0f, -1f, 4f);
      playerCharacter.transform.localRotation = Quaternion.Euler(0f, 180f, 0f);
      Tools.SetLayerToAllChildren(playerCharacter.transform, cameraView.gameObject.layer);
    }

    public void SetNextCharacter()
    {
      if(PlayerSetupManager.i.characters == null) return;

      recentCharacter++;
      if(recentCharacter == PlayerSetupManager.i.characters.Length)
        recentCharacter = 0;

      SetCharacter(recentCharacter);
    }

    public void SetPreviousCharacter()
    {
      if(cameraView == null || PlayerSetupManager.i.characters == null) return;

      recentCharacter--;
      if(recentCharacter == -1)
        recentCharacter = PlayerSetupManager.i.characters.Length - 1;

      SetCharacter(recentCharacter);
    }
}
