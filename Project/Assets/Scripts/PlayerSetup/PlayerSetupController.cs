using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using TMPro;

[RequireComponent(typeof(CharacterSelect))]
public class PlayerSetupController : MonoBehaviour
{
    private PlayerConfiguration playerConfig;
    private CharacterSelect characterSelect;

    [SerializeField] private TextMeshProUGUI playerStatusText;

    private void Start()
    {
      characterSelect = GetComponent<CharacterSelect>();
    }

    public void InitializeSetup(PlayerConfiguration playerConfig)
    {
      this.playerConfig = playerConfig;

      characterSelect.Initialize(playerConfig.index);
      TeamSelectManager.i.NewPlayerText(playerConfig.index);
      ReadyPlayerConfiguration(false);

      playerConfig.input.onActionTriggered += HandleAction;
    }

    public void RemoveAllListener()
    {
      playerConfig.input.onActionTriggered -= HandleAction;
    }

    private void HandleAction(InputAction.CallbackContext ctx)
    {
      if(ctx.action.name == "Move"){
        if(ctx.performed)
        {
          if(PlayerSetupManager.i.currentStep == 0)
            SelectCharacter(ctx.ReadValue<Vector2>());
          else if(PlayerSetupManager.i.currentStep == 1)
            SelectTeam(ctx.ReadValue<Vector2>());
        }
      }

      if(ctx.action.name == "Start") {
        if(ctx.performed && PlayerSetupManager.i.ContinueAllowed())
        {
          if(PlayerSetupManager.i.currentStep == 0)
          {
            PlayerSetupManager.i.SetNextStep();
            PlayerSetupManager.i.CheckPlayersConfiguration();
          } else if(PlayerSetupManager.i.currentStep == 1)
          {
            TeamSelectManager.i.SetSelectedTeam();
            PlayerSetupManager.i.SetNextStep();
          }

          AudioManager.SFXPlay("Click");
        }
      }

      if(ctx.action.name == "Submit") {
        if(ctx.performed && PlayerSetupManager.i.currentStep == 0)
        {
          ReadyPlayerConfiguration(true);
          PlayerSetupManager.i.CheckPlayersConfiguration();
        }
      }

      if(ctx.action.name == "Cancel") {
        if(ctx.performed)
        {
          if(PlayerSetupManager.i.currentStep == 0)
          {
            if(playerConfig.isReady)
              ReadyPlayerConfiguration(false);
            else
            {
              DDOL.Destroy();
              SceneManager.LoadScene("MainMenu");
            }
          } else if(PlayerSetupManager.i.currentStep == 1)
            PlayerSetupManager.i.SetPreviousStep();

          PlayerSetupManager.i.CheckPlayersConfiguration();
          AudioManager.SFXPlay("Cancel");
        }
      }
    }

    public void SelectCharacter(Vector2 inputArrow)
    {
        if(!playerConfig.isReady)
        {
            if(inputArrow.x > 0)
              characterSelect.SetNextCharacter();
            else if(inputArrow.x < 0)
              characterSelect.SetPreviousCharacter();

            AudioManager.SFXPlay("Switch");
        }
    }

    public void SelectTeam(Vector2 inputArrow)
    {
      if(inputArrow.x > 0)
      {
        if(playerConfig.selectedTeam == "Red Team")
        {
          TeamSelectManager.i.RemovePlayerConfig(playerConfig, "Red Team");
          PlayerSetupManager.i.CheckPlayersConfiguration();
          AudioManager.SFXPlay("Switch");
        }
        else if(playerConfig.selectedTeam == "None")
        {
          if(TeamSelectManager.blueTeamPlayers < 2)
          {
            TeamSelectManager.i.SelectTeam(playerConfig, "Blue Team");
            PlayerSetupManager.i.CheckPlayersConfiguration();
            AudioManager.SFXPlay("Switch");
          }
        }
      } else if(inputArrow.x < 0) {
        if(playerConfig.selectedTeam == "Blue Team")
        {
          TeamSelectManager.i.RemovePlayerConfig(playerConfig, "Blue Team");
          PlayerSetupManager.i.CheckPlayersConfiguration();
          AudioManager.SFXPlay("Switch");
        }
        else if(playerConfig.selectedTeam == "None")
        {
          if(TeamSelectManager.redTeamPlayers < 2)
          {
            TeamSelectManager.i.SelectTeam(playerConfig, "Red Team");
            PlayerSetupManager.i.CheckPlayersConfiguration();
            AudioManager.SFXPlay("Switch");
          }
        }
      }
    }

    public void ReadyPlayerConfiguration(bool val)
    {
      if(val) {
        playerConfig.isReady = true;
        playerStatusText.text = "<b>Ready!</b>";
        Debug.Log("PlayerConfiguration " + (playerConfig.index + 1) + " is Ready!");
        AudioManager.SFXPlay("Click");
      } else {
        playerConfig.isReady = false;
        playerStatusText.text = "Not Ready";
        Debug.Log("PlayerConfiguration " + (playerConfig.index + 1) + " is not Ready.");
        AudioManager.SFXPlay("Cancel");
      }
    }
}
