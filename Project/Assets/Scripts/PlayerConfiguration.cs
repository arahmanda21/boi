﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerConfiguration : MonoBehaviour
{
  public int index;
  public bool isReady;

  public PlayerInput input;
  public Character character;
  public string selectedTeam = "None";

  public void SetInput(PlayerInput playerInput)
  {
    input = playerInput;
    index = playerInput.playerIndex;
  }
}
