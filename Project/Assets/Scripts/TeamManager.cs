using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeamManager : MonoBehaviour
{
    private static bool switchToggle = true;

    public Team[] teams;

    public static TeamManager i;

    private void Awake()
    {
      if(i != null)
        Destroy(this);
      else
        i = this;
    }

    public void InitializeTeam(string teamName1, string teamName2)
    {
      teams = new Team[2]
      {
        new Team(teamName1),
        new Team(teamName2)
      };
    }

    public void SwapTeamRole()
    {
      teams[switchToggle ? 1 : 0].role = "Breaker";
      teams[switchToggle ? 0 : 1].role = "Builder";
      switchToggle = !switchToggle;
    }

    public void SetTeamRole(string teamName, string teamRole)
    {
      GetTeam(teamName).role = teamRole;
    }

    public Team GetTeam(string name)
    {
      foreach(Team team in teams)
        if(team.name == name) return team;
      return null;
    }

    public Team GetTeamByRole(string role)
    {
      foreach(Team team in teams)
        if(team.role == role) return team;
      return null;
    }

    public int GetPlayersCount(string teamName)
    {
      return GetTeam(teamName).playerConfigs.Count;
    }

    public int GetPlayersCountByRole(string teamRole)
    {
      return GetTeamByRole(teamRole).playerConfigs.Count;
    }

    public void AddPlayerToTeam(PlayerConfiguration playerConfig)
    {
      GetTeam(playerConfig.selectedTeam).playerConfigs.Add(playerConfig);
    }

    public void RemoveAllPlayerFromTeam()
    {
      foreach(Team team in teams)
        team.playerConfigs.Clear();
    }
}

[System.Serializable]
public class Team
{
  public string name;
  public string role;
  public List<PlayerConfiguration> playerConfigs;
  public int score;

  public Team(string teamName)
  {
    name = teamName;
    role = "";
    playerConfigs = new List<PlayerConfiguration>();
    score = 0;
  }
}
