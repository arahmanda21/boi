﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using TMPro;

public class FeedbackManager : MonoBehaviour
{
    [SerializeField] private TMP_InputField nameInput;
    [SerializeField] private TMP_InputField emailInput;
    [SerializeField] private TMP_InputField messageInput;
    [SerializeField] private TextMeshProUGUI statusText;
    [SerializeField] private Button sendButton;

    // Start is called before the first frame update
    void Start()
    {
      statusText.text = "";
      sendButton.onClick.AddListener(
        delegate{
          StartCoroutine(SendFeedback(nameInput.text, emailInput.text, messageInput.text));
        }
      );
    }

    IEnumerator SendFeedback(string name, string email, string message)
    {
      if(name.Length != 0 && email.Length != 0 && message.Length != 0)
      {
        if(ValidateEmail(email))
        {
          WWWForm form = new WWWForm();
          form.AddField("name", name);
          form.AddField("email", email);
          form.AddField("message", message);

          using (UnityWebRequest www = UnityWebRequest.Post("https://adityarahmanda.nasihosting.com/sendfeedback.php", form))
          {
            statusText.color = Color.black;
            statusText.text = "Sending...";

            www.SetRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0");
            yield return www.SendWebRequest();

            if(www.isNetworkError || www.isHttpError){
              Error("Network error, please check your internet connection!");
            } else {
              Success(www.downloadHandler.text);
            }
          }
        } else {
          Error("Your email is invalid.");
          yield return null;
        }
      } else {
        Error("You must complete all sections.");
        yield return null;
      }
    }

    private bool ValidateEmail(string email)
    {
      Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
      Match match = regex.Match(email);
      if(match.Success)
        return true;
      else
        return false;
    }

    private void Error(string errorText)
    {
      statusText.color = Color.red;
      statusText.text = errorText;
    }

    private void Success(string successText)
    {
      statusText.color = Color.black;
      statusText.text = successText;

      nameInput.text = "";
      emailInput.text = "";
      messageInput.text = "";
    }
}
