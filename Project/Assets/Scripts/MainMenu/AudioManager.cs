﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [System.Serializable]
    public class SoundAudioClip {
      public string name;
      public AudioClip audioClip;
    }

    public SoundAudioClip[] SoundAudioClipAssets;

    public AudioSource BGMAudioSource;
    public AudioSource[] SFXAudioSources;

    public static float waitTime;
    public static AudioManager instance;

    // Start is called before the first frame update
    void Awake()
    {
      if(instance != null)
      {
        Destroy(this);
      }
      else
      {
        instance = this;
        DontDestroyOnLoad(this);
      }
    }

    public static void SFXPlay(string name){
       instance.SFXAudioSources[0].PlayOneShot(GetAudioClip(name));
    }

    public static void SFXPlay(string name, int channel){
       instance.SFXAudioSources[channel].PlayOneShot(GetAudioClip(name));
    }

    public static void SFXPlayWithinSeconds(string name, float time, int channel){
        if(CanPlaySound(time)){
          instance.SFXAudioSources[channel].clip = GetAudioClip(name);
          instance.SFXAudioSources[channel].loop = false;
          instance.SFXAudioSources[channel].Play();
        }
    }

    public static void BGMPlay(string name){
      instance.BGMAudioSource.clip = GetAudioClip(name);
      instance.BGMAudioSource.loop = false;
      instance.BGMAudioSource.Play();
    }

    public static void BGMPlay(string name, bool loop){
        instance.BGMAudioSource.clip = GetAudioClip(name);
        instance.BGMAudioSource.loop = loop;
        instance.BGMAudioSource.Play();
    }

    public static void BGMStop(){
      instance.BGMAudioSource.loop = false;
      instance.BGMAudioSource.Stop();
      waitTime = 0f;
    }

    public static bool CanPlaySound(float time){
      if(waitTime > 0){
        waitTime -= Time.deltaTime;
        return false;
      } else {
        waitTime = time;
        return true;
      }
    }

    public static AudioClip GetAudioClip(string name){
      foreach(SoundAudioClip soundAudioClip in instance.SoundAudioClipAssets){
        if(soundAudioClip.name == name){
          return soundAudioClip.audioClip;
        }
      }
      return null;
    }
}
