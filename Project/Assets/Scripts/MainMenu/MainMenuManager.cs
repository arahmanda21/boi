﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.LowLevel;
using UnityEngine.InputSystem.Controls;
using UnityEngine.EventSystems;
using TMPro;

public class MainMenuManager : MonoBehaviour
{
  private static bool firstPlay = true;
  private bool anyKeyPressed;
  private bool toggleOnce;

  [SerializeField] private RectTransform logo;
  [SerializeField] private TextMeshProUGUI anyKeyText;
  [SerializeField] private CanvasGroup instructionCanvasGroup;

  [Header("Main Menu")]
  [SerializeField] private Canvas mainMenuCanvas;
  [SerializeField] private Button startButton;
  [SerializeField] private Button settingsButton;
  [SerializeField] private Button feedbackButton;
  [SerializeField] private Button creditsButton;
  [SerializeField] private Button exitButton;

  [Header("Settings Menu")]
  [SerializeField] private Canvas settingsCanvas;
  [SerializeField] private GameObject bgmSlider;
  [SerializeField] private Button settingsMainMenuButton;

  [Header("Feedback Menu")]
  [SerializeField] private Canvas feedbackCanvas;
  [SerializeField] private Button feedbackMainMenuButton;

  [Header("Credits Menu")]
  [SerializeField] private Canvas creditsCanvas;
  [SerializeField] private Button creditsMainMenuButton;

  [Header("Exit Dialogue Menu")]
  [SerializeField] private Canvas exitDialogueCanvas;
  [SerializeField] private Button yesButton;
  [SerializeField] private Button noButton;

  [Header("Others Settings")]
  [SerializeField] private Image whiteBackground;
  private float backgroundAlpha = 50f / 255f;

  private GameObject lastSelected;
  private CanvasGroup mainMenuCanvasGroup;
  private CanvasGroup settingsCanvasGroup;
  private CanvasGroup feedbackCanvasGroup;
  private CanvasGroup creditsCanvasGroup;
  private CanvasGroup exitDialogueCanvasGroup;

  void Start()
  {
    startButton.onClick.AddListener(
      delegate{
        LoadCharacterSelect();
        AudioManager.SFXPlay("Click");
      }
    );

    settingsButton.onClick.AddListener(
      delegate{
       whiteBackground.LeanAlphaImage(backgroundAlpha, 0.25f).setEase(LeanTweenType.easeOutCirc);
       OpenCanvas(settingsCanvas, settingsCanvasGroup, bgmSlider);
       mainMenuCanvasGroup.interactable = false;
       AudioManager.SFXPlay("Click");
     }
    );

    feedbackButton.onClick.AddListener(
      delegate{
       whiteBackground.LeanAlphaImage(backgroundAlpha, 0.25f).setEase(LeanTweenType.easeOutCirc);
       OpenCanvas(feedbackCanvas, feedbackCanvasGroup, feedbackMainMenuButton.gameObject);
       mainMenuCanvasGroup.interactable = false;
       AudioManager.SFXPlay("Click");
      }
    );

    creditsButton.onClick.AddListener(
      delegate{
       whiteBackground.LeanAlphaImage(backgroundAlpha, 0.25f).setEase(LeanTweenType.easeOutCirc);
       OpenCanvas(creditsCanvas, creditsCanvasGroup, creditsMainMenuButton.gameObject);
       mainMenuCanvasGroup.interactable = false;
       AudioManager.SFXPlay("Click");
      }
    );

    exitButton.onClick.AddListener(
      delegate{
       whiteBackground.LeanAlphaImage(backgroundAlpha, 0.25f).setEase(LeanTweenType.easeOutCirc);
       OpenCanvas(exitDialogueCanvas, exitDialogueCanvasGroup, noButton.gameObject);
       mainMenuCanvasGroup.interactable = false;
       AudioManager.SFXPlay("Click");
      }
    );

    settingsMainMenuButton.onClick.AddListener(
      delegate {
        whiteBackground.LeanAlphaImage(0f, 0.25f).setDelay(0.25f).setEase(LeanTweenType.easeInCirc);
        CloseCanvas(settingsCanvas, settingsCanvasGroup, settingsButton.gameObject);
        mainMenuCanvasGroup.interactable = true;
        AudioManager.SFXPlay("Click");
      }
    );

    feedbackMainMenuButton.onClick.AddListener(
      delegate {
        whiteBackground.LeanAlphaImage(0f, 0.25f).setDelay(0.25f).setEase(LeanTweenType.easeInCirc);
        CloseCanvas(feedbackCanvas, feedbackCanvasGroup, feedbackButton.gameObject);
        mainMenuCanvasGroup.interactable = true;
        AudioManager.SFXPlay("Click");
      }
    );

    creditsMainMenuButton.onClick.AddListener(
      delegate {
        whiteBackground.LeanAlphaImage(0f, 0.25f).setDelay(0.25f).setEase(LeanTweenType.easeInCirc);
        CloseCanvas(creditsCanvas, creditsCanvasGroup, creditsButton.gameObject);
        mainMenuCanvasGroup.interactable = true;
        AudioManager.SFXPlay("Click");
      }
    );

    noButton.onClick.AddListener(
      delegate {
        whiteBackground.LeanAlphaImage(0f, 0.25f).setDelay(0.25f).setEase(LeanTweenType.easeInCirc);
        CloseCanvas(exitDialogueCanvas, exitDialogueCanvasGroup, exitButton.gameObject);
        mainMenuCanvasGroup.interactable = true;
        AudioManager.SFXPlay("Click");
      }
    );

    yesButton.onClick.AddListener(Application.Quit);

    mainMenuCanvasGroup = mainMenuCanvas.GetComponent<CanvasGroup>();
    settingsCanvasGroup = settingsCanvas.GetComponent<CanvasGroup>();
    feedbackCanvasGroup = feedbackCanvas.GetComponent<CanvasGroup>();
    creditsCanvasGroup = creditsCanvas.GetComponent<CanvasGroup>();
    exitDialogueCanvasGroup = exitDialogueCanvas.GetComponent<CanvasGroup>();

    if(GlobalConfiguration.i.firstPlay)
    {
      AddListenerToAnyKey();
      AudioManager.BGMPlay("Lucid", true);
      GlobalConfiguration.i.firstPlay = false;
    } else {
      LeanTween.moveY(logo, 170f, 0f);
      anyKeyText.LeanAlphaText(0f, 0f);
      OpenCanvas(mainMenuCanvas, mainMenuCanvasGroup, startButton.gameObject);
      LeanTween.alphaCanvas(instructionCanvasGroup, 1f, 1f).setEase(LeanTweenType.easeOutSine);
    }
  }

  void Update()
  {
    if(toggleOnce && anyKeyPressed)
    {
      AudioManager.SFXPlay("Click");
      LeanTween.moveY(logo, 170f, 1f).setEase(LeanTweenType.easeOutSine).setDelay(0.25f)
               .setOnComplete(() => {
                 OpenCanvas(mainMenuCanvas, mainMenuCanvasGroup, startButton.gameObject);
                 LeanTween.alphaCanvas(instructionCanvasGroup, 1f, 1f).setEase(LeanTweenType.easeOutSine);}
      );
      anyKeyText.LeanAlphaText(0f, 0.25f).setEase(LeanTweenType.easeOutSine);
      anyKeyPressed = false;
    }
  }

  private void AddListenerToAnyKey()
  {
    InputSystem.onEvent +=
      (eventPtr, device) =>
      {
        if(!eventPtr.IsA<StateEvent>() && !eventPtr.IsA<DeltaStateEvent>()) return;

        var controls = device.allControls;
        var buttonPressPoint = InputSystem.settings.defaultButtonPressPoint;
        for(var i = 0; i < controls.Count; ++i)
        {
          var control = controls[i] as ButtonControl;
          if(control == null || control.synthetic || control.noisy)
            continue;
          if(!toggleOnce && control.ReadValueFromEvent(eventPtr, out var value) && value >= buttonPressPoint)
          {
            anyKeyPressed = true;
            toggleOnce = true;
            break;
          }
        }
      };
  }

  private void FadeInAnyKey()
  {
    anyKeyText.LeanAlphaText(0f, 0.5f).setDelay(1f).setEase(LeanTweenType.easeOutSine).setOnComplete(FadeOutAnyKey);
  }

  private void FadeOutAnyKey()
  {
    anyKeyText.LeanAlphaText(1f, 0.5f).setEase(LeanTweenType.easeInSine).setOnComplete(FadeInAnyKey);
  }

  private void OpenCanvas(Canvas _canvas, CanvasGroup _canvasGroup, GameObject firstSelected)
  {
      _canvas.enabled = true;
      LeanTween.scale(_canvas.gameObject, Vector3.one, 0.25f)
               .setDelay(0.25f)
               .setEase(LeanTweenType.easeOutCirc)
               .setOnComplete(() => SelectedGameObject(firstSelected));
      //LeanTween.alphaCanvas(_canvasGroup, 1f, 1f)
      //         .setEase(LeanTweenType.easeOutSine)
      //         .setOnComplete(() => SelectedGameObject(firstSelected));
      _canvasGroup.interactable = true;
  }

  private void CloseCanvas(Canvas _canvas, CanvasGroup _canvasGroup, GameObject firstSelected)
  {
      LeanTween.scale(_canvas.gameObject, Vector3.zero, 0.25f)
               .setEase(LeanTweenType.easeInCirc)
               .setOnComplete(() => {
                 SelectedGameObject(firstSelected);
                 _canvas.enabled = false;
                 }
                );
      //LeanTween.alphaCanvas(_canvasGroup, 0f, 1f).setEase(LeanTweenType.easeOutSine)
      //         .setOnComplete(() => {SelectedGameObject(firstSelected); _canvas.enabled = false;});
      _canvasGroup.interactable = false;
  }

  private void SelectedGameObject(GameObject _selectedGameObject)
  {
    EventSystem.current.SetSelectedGameObject(null);
    EventSystem.current.SetSelectedGameObject(_selectedGameObject);
  }

  private void LoadCharacterSelect()
  {
    SceneManager.LoadScene("PlayerSetup");
  }
}
