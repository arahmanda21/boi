﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioSetting : MonoBehaviour
{
    public float sfxMaxVolume = 0.5f;
    private float sfx_const = 1f;

    [SerializeField] private Slider bgmSlider;
    [SerializeField] private Slider sfxSlider;

    private AudioSource bgmAudio;
    private AudioSource[] sfxAudio;

    // Start is called before the first frame update
    void Start()
    {
        bgmAudio = AudioManager.instance.BGMAudioSource;
        sfxAudio = AudioManager.instance.SFXAudioSources;

        bgmSlider.onValueChanged.AddListener(delegate{UpdateBGMVolume();});
        sfxSlider.onValueChanged.AddListener(delegate{UpdateSFXVolume();});

        sfx_const = sfxMaxVolume / 1f;
        for(int i = 0; i < sfxAudio.Length; i++)
          sfxAudio[i].volume = sfxMaxVolume;
    }

    private void UpdateBGMVolume()
    {
        bgmAudio.volume = bgmSlider.value;
    }

    public void UpdateSFXVolume()
    {
        for(int i = 0; i < sfxAudio.Length; i++)
          sfxAudio[i].volume = sfxSlider.value * sfx_const;
    }
}
