﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [Header("Player Configuration")]
    public PlayerConfiguration config;
    public string role;

    [Header("Player Behaviours")]
    public MovementBehaviour movementBehaviour;
    public BreakerBehaviour breakerBehaviour;
    public BuilderBehaviour builderBehaviour;

    [Header("Other Settings")]
    public ParticleSystem dieParticleSystem;
    public TeamIndicator teamIndicator;

    private const float FALL_DEPTH_LIMIT = -35f;

    private void Update()
    {
      if(transform.position.y < FALL_DEPTH_LIMIT)
      {
        RemoveAllListener();
        if(role == "Breaker")
          GameManager.BreakerDead(this);
        else if(role == "Builder")
          GameManager.BuilderDead(this);
        Destroy(gameObject);
      }
    }

    public void RemoveAllListener()
    {
      if(role == "Breaker")
        breakerBehaviour.RemoveInputListener();
      else if(role == "Builder")
        builderBehaviour.RemoveInputListener();
    }

    public void SetBehaviour(PlayerConfiguration playerConfig, PlayerBehaviours playerBehaviours, string role)
    {
        config = playerConfig;
        this.role = role;
        Tools.SetTag(gameObject, role);

        movementBehaviour = Tools.CopyComponent<MovementBehaviour>(playerBehaviours.movementBehaviour, gameObject);
        movementBehaviour.FindMissingReferences();

        if(role == "Breaker")
        {
          breakerBehaviour = Tools.CopyComponent<BreakerBehaviour>(playerBehaviours.breakerBehaviour, gameObject);
          breakerBehaviour.FindMissingReferences();
          breakerBehaviour.AddInputListener();
        }
        else if(role == "Builder")
        {
          builderBehaviour = Tools.CopyComponent<BuilderBehaviour>(playerBehaviours.builderBehaviour, gameObject);
          builderBehaviour.FindMissingReferences();
          builderBehaviour.AddInputListener();
        }
    }

    public void Die()
    {
      dieParticleSystem.transform.parent = null;
      dieParticleSystem.Play();
      AudioManager.SFXPlay("Die");
      Destroy(this.gameObject);
    }
}
