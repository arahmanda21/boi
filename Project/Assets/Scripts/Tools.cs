﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tools
{
    public static void SetLayerToAllChildren(Transform trans, int layer)
    {
        trans.gameObject.layer = layer;
        foreach(Transform child in trans)
            SetLayerToAllChildren(child, layer);
    }

    public static void SetTag(GameObject go, string tag)
    {
        go.tag = tag;
    }

    public static Transform GetChildTransformWithName(Transform trans, string name) {
      Transform[] children = trans.GetComponentsInChildren<Transform>();

      foreach(Transform child in children) {
        if(child.name == name) return child;
      }

      return null;
    }

    public static Vector3 RandomizePos(Vector3 position, float randomOffset)
    {
      float randomPosX = position.x + Random.Range(0, randomOffset);
      float randomPosZ = position.z + Random.Range(0, randomOffset);
      Vector3 result = new Vector3(randomPosX, position.y, randomPosZ);

      return result;
    }

    public static void ResetPosition(Transform trans)
    {
      trans.localRotation = Quaternion.identity;
      trans.localPosition = Vector3.zero;
    }

    public static T CopyComponent<T>(T original, GameObject destination) where T : Component
    {
      System.Type type = original.GetType();
      Component copy = destination.AddComponent(type);
      System.Reflection.FieldInfo[] fields = type.GetFields();
      foreach (System.Reflection.FieldInfo field in fields)
      {
          field.SetValue(copy, field.GetValue(original));
      }
      return copy as T;
    }
}
