﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalConfiguration : MonoBehaviour
{
    public bool gamePaused = false;
    public bool gameStarted = false;
    public bool gameEnded = true;
    public bool firstPlay = true;

    public static GlobalConfiguration i;

    void Awake()
    {
      if(i != null)
        Destroy(this);
      else
      {
        i = this;
        DontDestroyOnLoad(this);
      }
    }
}
