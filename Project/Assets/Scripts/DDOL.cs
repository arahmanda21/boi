﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DDOL : MonoBehaviour
{
    private static GameObject _gameObject;

    // Start is called before the first frame update
    void Awake()
    {
      _gameObject = this.gameObject;
      DontDestroyOnLoad(_gameObject);
    }

    public static void Destroy()
    {
      Destroy(_gameObject);
    }
}
