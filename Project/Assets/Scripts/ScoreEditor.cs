﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

#if UNITY_EDITOR
[CustomEditor(typeof(ScoreBarBehaviour))]
public class ScoreEditor : Editor
{
	override public void OnInspectorGUI()
	{
     serializedObject.Update();

     EditorGUILayout.PropertyField(serializedObject.FindProperty("value"));
     EditorGUILayout.PropertyField(serializedObject.FindProperty("offsetX"));
     EditorGUILayout.PropertyField(serializedObject.FindProperty("originBar"));

     EditorGUILayout.PropertyField(serializedObject.FindProperty("useColor"));
     if (serializedObject.FindProperty("useColor").boolValue) {
       EditorGUILayout.PropertyField(serializedObject.FindProperty("activeColor"));
 		 } else {
       EditorGUILayout.PropertyField(serializedObject.FindProperty("activeBar"));
     }
     EditorGUILayout.PropertyField(serializedObject.FindProperty("reverseBar"));

    serializedObject.ApplyModifiedProperties();
	}
}
#endif
